//
//  NoteCollectionViewCell.swift
//  Coredata-Homework
//
//  Created by Hoeng Linghor on 12/5/20.
//

import UIKit

class NoteCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var des: UILabel!
    var note:Note?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        title.text = note?.title
        des.text = note?.des

    }
}
