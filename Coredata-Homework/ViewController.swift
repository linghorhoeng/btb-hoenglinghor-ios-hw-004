//
//  ViewController.swift
//  Coredata-Homework
//
//  Created by Hoeng Linghor on 12/5/20.
//

import UIKit
import CoreData

class ViewController: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var noteView: UICollectionView!
    
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var notes : [Note]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        navigationItem.title = "Note".localizedString()
        registerCell()
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        lpgr.minimumPressDuration = 0.5
        lpgr.delaysTouchesBegan = true
        lpgr.delegate = self
        self.noteView.addGestureRecognizer(lpgr)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getNotes()
    }
    
    func registerCell(){
        noteView.register(UINib(nibName: "NoteCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "noteCell")
    }
    
    func getNotes() {
        self.notes = try? context.fetch(Note.fetchRequest())
        self.noteView.reloadData()
    }
    func setLang(lang: String){
        AppService.shared.choose(language: lang)
        navigationItem.title = "Note".localizedString()

       
    }
    @IBAction func changeLang(_ sender: Any) {
        let alertAction = UIAlertController( title: nil, message: "Do you want to change language?".localizedString(), preferredStyle: .actionSheet)
        
       
        let khAction = UIAlertAction(title: "Change to Khmer".localizedString(), style: .destructive, handler: { action in
                self.setLang(lang: language.khmer.rawValue)
            })
            // Configure Cancel Action Sheet
        let enAction = UIAlertAction(title: "Change to English".localizedString(), style: .cancel, handler: { acion in
                self.setLang(lang: language.english.rawValue)
            })
            alertAction.addAction(khAction)
            alertAction.addAction(enAction)
            self.present(alertAction, animated: true, completion: nil)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "navToEdit" {
            let dest = segue.destination as! AddViewController
            dest.note = sender as? Note
        }
        
    }
    //MARK: - UILongPressGestureRecognizer Action -
    
    @objc func handleLongPress(gestureReconizer: UILongPressGestureRecognizer) {
        let alertActionCell = UIAlertController( title: nil, message: "Do you want to delete this note?".localizedString(), preferredStyle: .actionSheet)
        if gestureReconizer.state != UIGestureRecognizer.State.ended {
            return
        }
        
        let p = gestureReconizer.location(in: noteView)
        if let indexPath = self.noteView.indexPathForItem(at: p){
            let deleteAction = UIAlertAction(title: "Delete".localizedString(), style: .destructive, handler: { action in
                
                let note = self.notes![indexPath.row]
                self.context.delete(note)
                self.getNotes()
                
                print("Note Removed")
            })
            // Configure Cancel Action Sheet
            let cancelAction = UIAlertAction(title: "Cancel".localizedString(), style: .cancel, handler: { acion in
                print("Cancel actionsheet")
            })
            alertActionCell.addAction(deleteAction)
            alertActionCell.addAction(cancelAction)
            self.present(alertActionCell, animated: true, completion: nil)
            
            self.noteView.reloadData()

        }
        
    }
    
}

extension ViewController: UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notes?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "noteCell", for: indexPath) as! NoteCollectionViewCell
        cell.title?.text = notes?[indexPath.row].title
        cell.des?.text = notes?[indexPath.row].des
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        cell.addGestureRecognizer(lpgr)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let note = self.notes![indexPath.row]
        self.performSegue(withIdentifier: "navToEdit", sender: note)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 170, height: 210)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 20, bottom: 0, right: 20)
    }
}
extension String {
    func localizedString()->String {
        let path = Bundle.main.path(forResource: AppService.shared.language, ofType: "lproj")!
        let bundle = Bundle(path: path)!
        return NSLocalizedString(self, tableName: nil, bundle: bundle, value: self, comment: self)
    }
}
