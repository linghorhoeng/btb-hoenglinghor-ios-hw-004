//
//  AddViewController.swift
//  Coredata-Homework
//
//  Created by Hoeng Linghor on 12/5/20.
//

import UIKit
import CoreData

class AddViewController: UIViewController,UINavigationControllerDelegate {
    
    @IBOutlet weak var desText: UITextField!
    @IBOutlet weak var titleText: UITextField!
    
    
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var note: Note?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Write New Note".localizedString()
        // Do any additional setup after loading the view.
        if let note = self.note{
            navigationItem.title = "Edit Note".localizedString()
            titleText.text = note.title
            desText.text = note.des
        }
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Note".localizedString(), style: .done, target: self, action: #selector(backToInitial(sender:)))
        
    }
    
    @IBAction func leftClick(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let takePhoto = UIAlertAction(title: "Take Photo".localizedString(), style: .default) { (action) in
            print("take photo")
        }
        let choosePhoto = UIAlertAction(title: "Choose Image".localizedString(), style: .default) { (action) in
            print("Choose photo")
        }
        let drawing = UIAlertAction(title: "Drawing".localizedString(), style: .default) { (action) in
            print("Drawing")
        }
        let recording = UIAlertAction(title: "Recording".localizedString(), style: .default) { (action) in
            print("Recording")
        }
        let checkbox = UIAlertAction(title: "Checkboxs".localizedString(), style: .default) { (action) in
            print("checkbox")
        }
        let cancel = UIAlertAction(title: "Cancle".localizedString(), style: .cancel) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(takePhoto)
        alert.addAction(choosePhoto)
        alert.addAction(drawing)
        alert.addAction(recording)
        alert.addAction(checkbox)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func rightClick(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let delete = UIAlertAction(title: "Delete".localizedString(), style: .default) { (action) in
            print("Delete")
        }
        let makeCopy = UIAlertAction(title: "Make A Copy".localizedString(), style: .default) { (action) in
            print("makeCopy")
        }
        let send = UIAlertAction(title: "Send".localizedString(), style: .default) { (action) in
            print("send")
        }
        let collabrators = UIAlertAction(title: "Collabrators".localizedString(), style: .default) { (action) in
            print("collabrators")
        }
        let labels = UIAlertAction(title: "Labels".localizedString(), style: .default) { (action) in
            print("labels")
        }
        let cancel = UIAlertAction(title: "Cancle".localizedString(), style: .cancel) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(delete)
        alert.addAction(makeCopy)
        alert.addAction(send)
        alert.addAction(collabrators)
        alert.addAction(labels)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)

    }
    
    @objc func backToInitial(sender: AnyObject) {
        
        if let note = self.note{
            note.title = titleText.text
            note.des = desText.text
            
        } else {
            let note = Note(context: self.context)
            note.title = titleText.text
            note.des = desText.text
        }
        appDelegate.saveContext()
        self.navigationController?.popToRootViewController(animated: true)
    }
}
